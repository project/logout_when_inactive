
-- SUMMARY --

The Drupal Logout When Inactive module logout the current user when Inactive for Certain time.

For a full description of the module, visit the project page:
  http://drupal.org/project/logout_when_inactive

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/logout_when_inactive


-- REQUIREMENTS --

None.


-- INSTALLATION --


-- CONFIGURATION --



-- CUSTOMIZATION --



-- TROUBLESHOOTING --



-- FAQ --



-- CONTACT --


