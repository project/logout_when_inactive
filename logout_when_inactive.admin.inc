<?php
/**
 * @file
 * Menu callback include file for admin/settings pages.
 */

/**
 * Implementation of menu callback.
 */
function logout_when_inactive_admin_settings($form, &$form_state) {
    global $user;
    
    
    $vars = array(
        'expiry_enabled' => array(
            '#type' => 'item',
            '#title' => t('Auto Logout Enabled:'),
            '#value' => variable_get('expiry_enabled', TRUE) ? t('Yes') : t('No'),
            '#description' => t('Does auto logout enabled or disabled.'),
        ),
        'expiry_forced' => array(
            '#type' => 'item',
            '#title' => t('Forced Logout Enabled:'),
            '#value' => variable_get('expiry_forced', FALSE) ? t('Yes') : t('No'),
            '#description' => t('Does auto logout enabled or disabled.'),
        ),
        'expiry_period' => array(
            '#type' => 'item',
            '#title' => t('Expiry Period :'),
            '#value' => format_interval(variable_get('expiry_period', 900)),
            '#description' => t('Time period when page will logout.'),
        )
    );
    $form['legend'] = array(
        '#type' => 'markup',
        '#markup' => theme('logout_when_inactive_settings_display', array('vars' => $vars)),
    );

    $form['expiry_enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable auto logout'),
        '#default_value' => variable_get('expiry_enabled', TRUE),
    );
    $form['expiry_forced'] = array(
        '#type' => 'checkbox',
        '#title' => t('Force logout?'),
        '#default_value' => variable_get('expiry_forced', FALSE),
        '#description' => t("If checked, then it will logout even when the user is active,")."<br />".t("If un-checked, then will sense everyclick of the user."),
    );
    $form['expiry_period'] = array(
        '#type' => 'textfield',
        '#title' => t('Expiry period'),
        '#default_value' => variable_get('expiry_period', '900'),
        '#description' => t("Expiry period in seconds.")."<br />".t("After this interval logout will happen silently."),
    );

    return system_settings_form($form);
}

/**
 * Theme callback function to dispaly status information.
 */
function theme_logout_when_inactive_settings_display($variables) {
    $rows = array(array());

    foreach ($variables['vars'] as $var) {
        $element = array(
            'element' => array(
                '#type' => 'markup',
                '#children' => $var['#value'],
                '#title' => $var['#title'],
                '#title_display' => 'before',
            )
        );

        $output = '<div class="container-inline">' . theme('form_element', $element) . '</div>';
        $output .= '<div class="description">' . $var['#description'] . '</div>';

        $rows[0][] = array('data' => $output);
    }
    return theme('table', array('rows' => $rows));
}
